/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     18/12/2021 16:43:53                          */
/*==============================================================*/



/*==============================================================*/
/* Table: _CODE_HORARIO                                         */
/*==============================================================*/
create table HORARIO
(
   ID_HORARIO           int not null auto_increment,
   ID_MATERIA           int not null,
   DIA_S                varchar(50),
   HORA_INI             time,
   HORA_FIN             time,
   primary key (ID_HORARIO)
);

/*==============================================================*/
/* Table: _CODE_HORARIO_ASIGNADO                                */
/*==============================================================*/
create table HORARIO_ASIGNADO
(
   ID_HORARIO           int not null,
   ID_USUARIO           int not null,
   primary key (ID_HORARIO, ID_USUARIO)
);

/*==============================================================*/
/* Table: _CODE_MATERIA                                         */
/*==============================================================*/
create table MATERIA
(
   ID_MATERIA           int not null auto_increment,
   ID_NIVEL             int not null,
   NOMB_MATERIA         varchar(50),
   primary key (ID_MATERIA)
);

/*==============================================================*/
/* Table: _CODE_NIVEL                                           */
/*==============================================================*/
create table NIVEL
(
   ID_NIVEL             int not null auto_increment,
   NOMB_NIVEL           varchar(50),
   primary key (ID_NIVEL)
);

/*==============================================================*/
/* Table: PARALELO                                        */
/*==============================================================*/
create table PARALELO
(
   ID_PARALELO          int not null auto_increment,
   ID_NIVEL             int not null,
   PARALELO             varchar(20),
   primary key (ID_PARALELO)
);

/*==============================================================*/
/* Table: _CODE_ROL                                             */
/*==============================================================*/
create table ROL
(
   ID_ROL               int not null auto_increment,
   NOMBRE_ROL           varchar(20),
   primary key (ID_ROL)
);

/*==============================================================*/
/* Table: _CODE_USUARIO                                         */
/*==============================================================*/
create table USUARIO
(
   ID_USUARIO           int not null auto_increment,
   ID_ROL               int,
   NOMBRE               varchar(20),
   APELLIDO             varchar(20),
   NOMBRE_USUARIO       varchar(20),
   CORREO				varchar(50),
   CONTRASENA           varchar(200),
   DIRECCION            varchar(50),
   primary key (ID_USUARIO)
);

alter table HORARIO add constraint FK_RELATIONSHIP_3 foreign key (ID_MATERIA)
      references MATERIA (ID_MATERIA) on delete restrict on update restrict;

alter table HORARIO_ASIGNADO add constraint FK_RELATIONSHIP_2 foreign key (ID_HORARIO)
      references HORARIO (ID_HORARIO) on delete restrict on update restrict;

alter table HORARIO_ASIGNADO add constraint FK_RELATIONSHIP_6 foreign key (ID_USUARIO)
      references USUARIO (ID_USUARIO) on delete restrict on update restrict;

alter table MATERIA add constraint FK_RELATIONSHIP_4 foreign key (ID_NIVEL)
      references NIVEL (ID_NIVEL) on delete restrict on update restrict;

alter table PARALELO add constraint FK_RELATIONSHIP_5 foreign key (ID_NIVEL)
      references NIVEL (ID_NIVEL) on delete restrict on update restrict;

alter table USUARIO add constraint FK_RELATIONSHIP_7 foreign key (ID_ROL)
      references ROL (ID_ROL) on delete restrict on update restrict;
INSERT INTO ROL (NOMBRE_ROL) VALUES 
						('ESTUDIANTE'),
						('DOCENTE');

						
						

INSERT INTO USUARIO (ID_ROL, NOMBRE,APELLIDO,NOMBRE_USUARIO, CORREO, CONTRASENA,DIRECCION) VALUES
												(1,'FERNANDO','CARDENAS', 'fercar','fcar@gmail.com', 'Estud2021', 'COSTA AZUL'),
												(2,'TATIANA','MACÍAS','tatmac', 'tmac@gmail.com','Prof2021','LOS ESTEROS'),
												(1,'JAIME','DUARTE', 'jaidua', 'jdua@gmail.com', 'Estud2021','COSTA AZUL'),
												(2,'MONSERRATE','LUCAS','monluc', 'mluc@gmail.com','Prof2021','LOS ESTEROS'),
												(1,'PETER','LOPEZ', 'petlop', 'bloo@gmail.com','Estud2021','LA AURORA'),
												(1,'MARIA','ORDOÑEZ','marord','mord@gmail.com','Estud2021','LAS MARIAS'),
												(1,'FERNANDO','FLORES','fernanfloo','fflo@gmail.com','Estud2021','JOCAY'),
												(1,'MARIA','GUERRERO','margue','mgue@gmail.com','Estud2021','LAS CUMBRES'),
												(2,'DIEGO','CHICA','diechi','dchi@gmail.com','Prof2021','15 DE SEPTIEMBRE'),
												(1,'MARCELA','CARDONA', 'marcar','mcarD@gmail.com','Estud2021','CORDOVA');

												
INSERT INTO NIVEL (NOMB_NIVEL) VALUES ('PRIMER NIVEL'), ('SEGUNDO NIVEL');
INSERT INTO PARALELO (ID_NIVEL, PARALELO) VALUES
						(1,'A'),(1,'B'),(2,'A'),(2,'B');

INSERT INTO MATERIA (ID_NIVEL,NOMB_MATERIA) VALUES 
						(1,'FÍSICA'),(1,'ALGEBRA'),(1,'FUNDAMENTOS DE PROGRAMACIÓN'),(1,'FUNDAMENTOS DE TI'),
						(2,'ESTRUCTURA DE DATOS'),(2,'SISTEMAS ELECTRÍCOS'),(2,'CÁLCULO DE UNA VARIABLE'),
						(2,'MATEMÁTICAS DDISCRETAS');
						
INSERT INTO HORARIO (ID_MATERIA,DIA_S,HORA_INI,HORA_FIN) VALUES
						(1,'LUNES, MARTES, MIERCOLES','08:00','10:00'),(1,'MIERCOLES, JUEVES, VIERNES','15:00','17:00'),
						(2,'LUNES, MARTES','10:00','12:00'),(2,'LUNES, MARTES', '15:00', '17:00'),
						(3,'MIERCOLES','10:00','12:00'),(3,'MIERCOLES','17:00','19:00'),
						(4,'JUEVES, VIERNES','08:00','10:00'),(4,'JUEVES, VIERNES','17:00', '19:00'),
						(5,'LUNES, MARTES, MIERCOLES','08:00','10:00'),(5,'MIERCOLES, JUEVES, VIERNES','15:00','17:00'),
						(6,'LUNES, MARTES','10:00','12:00'),(6,'LUNES, MARTES', '15:00', '17:00'),
						(7,'MIERCOLES','10:00','12:00'),(7,'MIERCOLES','17:00','19:00'),
						(8,'JUEVES, VIERNES','08:00','10:00'),(8,'JUEVES, VIERNES','17:00', '19:00');
						
/*INSERT INTO HORARIO_ASIGNADO (ID_HORARIO,ID_USUARIO) VALUES
								(),/* HASTA AQUÍ LLEGUÉ XD PORQUE SIMPLMENTE ES DISTRIBUIR DESDE EL USUARIO 5 HASTA EL ULTIMO
									EN LOS DIFERENTES PALELOS, DE CADA NIVEL Y YA, PERO PARA ESO NECESITABA EL DISEÑO CORRGIDO.
									PERO AHÍ CREO QUE LO DEMÁS ESTÁ BIEN ;)
								*/
											
